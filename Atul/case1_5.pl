:-ensure_loaded('support1_5.pl').

%1 Bill travelled to the office.
%2 Bill picked up the football there.
%3 Bill went to the bedroom.
%4 Bill gave the football to Fred.
%5 What did Bill give to Fred? 	football	4
%6 Fred handed the football to Bill.
%7 Jeff went back to the office.
%8 Who received the football? 	Bill	6

    happens(move(bill,office),1).
    happens(pickup(bill,football),2).
    happens(move(bill,bedroom),3).
    happens(tp(football,bill,fred),4).
    happens(tp(football,fred,bill),5).
    happens(move(jeff,office),6).


%5 What did Bill give to Fred? 	football	4
test1(Object):-
    basics:for(T1,1,4),
    holds(tp(Object,bill,fred),T1).


    
%8 Who received the football? 	Bill	6
test2(Recipient):-
    basics:for(T2,5,6),
    holds(tp(football,fred,Recipient),T2).    
    
