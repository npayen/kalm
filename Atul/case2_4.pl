:-ensure_loaded('support2_4.pl').

%1 The kitchen is west of the garden.
%2 The hallway is west of the kitchen.
%3 What is the garden east of?	kitchen	1

west(kitchen,garden).
west(hallway,kitchen).


% What is the garden east of?   kitchen
test1(Y):-
    is_east(garden,Y).
