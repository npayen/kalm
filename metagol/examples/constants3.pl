:- if(current_prolog_flag(dialect,xsb)). %% need for portability
?- (current_module(metagol) -> true
   ; abort('Metagol must be loaded before examples.')
   ).
:- elif(current_prolog_flag(dialect,swi)).
:- use_module('../metagol').
:- endif.
:- dynamic body_pred/1, head_pred/1.


%% tell metagol to use the BK
body_pred(p/2).

%% metarules
metarule([P,Q,X,Y], [P,X,A], [[Q,Y,A]]).

%% background knowledge
p(spongebob,laura).
p(patrick,amelia).


:-
    learn([f(andy,laura),f(andy,amelia)],[]).
