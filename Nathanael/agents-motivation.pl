/*
  Task 20: Agent's Motivation
1 Jason is thirsty.
2 Where will jason go?	kitchen	1
3 Antoine is bored.
4 Where will antoine go?	garden	3
5 Jason went to the kitchen.
6 Why did jason go to the kitchen?	thirsty	1
7 Antoine journeyed to the garden.
8 Why did antoine go to the garden?	bored	3
9 Sumit is hungry.
10 Where will sumit go?	kitchen	9
11 Antoine grabbed the football there.
12 Why did antoine get the football?	bored	3
13 Sumit went to the kitchen.
14 Why did sumit go to the kitchen?	hungry	9
15 Jason took the milk there.
16 Why did jason get the milk?	thirsty	1
17 Sumit took the apple there.
18 Why did sumit get the apple?	hungry	9
19 Yann is thirsty.
20 Where will yann go?	kitchen	19
21 Yann travelled to the kitchen.
22 Why did yann go to the kitchen?	thirsty	19
*/

happens(motivation(jason,thirsty),1).
happens(motivation(antoine,bored),2).
happens(move(jason,kitchen),3).
happens(move(antoine,garden),4).
happens(motivation(sumit,hungry),5).
happens(get(antoine,football),6).
happens(move(sumit,kitchen),7).
happens(get(jason,milk),8).
happens(get(sumit,apple),9).
happens(motivation(yann,thirsty),10).
happens(move(yann,kitchen),11).




motives(thirsty,kitchen).
motives(bored,garden).
motives(hungry,kitchen).
motives(bored,football).
motives(hungry,apple).
motives(tired,bedroom).
motives(tired,pajamas).

place(X) :-
  basics:member(X,[kitchen,garden,bedroom]).

item(X) :-
  basics:member(X,[football,apple,pajamas]).

initiates(motivation(A,M),wantmove(A,L)) :-
  motives(M,L),
  place(L).

initiates(motivation(A,M),wantitem(A,I)) :-
  motives(M,I),
  item(I).

initiates(move(A,P),reason(A,M)) :-
  place(P),
  motives(M,P).

initiates(get(A,I),reason(A,M)) :-
  item(I),
  motives(M,I).

initiates(move(A,P), loc(A,P)).

terminates(move(A,P1), loc(A,P2)):-
  place(P2),
	P2 \= P1.

initiates(get(P,O), holding(P,O)).

terminates(putdown(P,O), holding(P,O)).

holds(Fact, Time2) :-
	initiates(Evnt,Fact),
	happens(Evnt,Time1),
	Time1 =< Time2,
	\+ (
		happens(Evnt2, Time3),
		terminates(Evnt2, Fact),
		Time1 =< Time3,
		Time3 =< Time2
	).

%Where will jason go? kitchen
t1(T,P) :-
	basics:for(T,1,11),
	holds(wantmove(jason,P),T).

%Where will antoine go? garden
t2(T,P) :-
	basics:for(T,1,11),
	holds(wantmove(antoine,P),T).

%Why did jason go to the kitchen? thirsty
t3(T,M) :-
  basics:for(T,1,11),
  holds(loc(jason,kitchen),T),
  holds(reason(jason,M),T).

%Why did antoine go to the garden? bored
t4(T,M) :-
  basics:for(T,1,11),
  holds(loc(antoine,garden),T),
  holds(reason(antoine,M),T).

%Where will sumit go? kitchen
t5(T,P) :-
	basics:for(T,1,11),
	holds(wantmove(sumit,P),T).

%Why did antoine get the football? bored
t6(T,M) :-
  basics:for(T,1,11),
  holds(holding(antoine,football),T),
  holds(reason(antoine,M),T).

%Why did sumit go to the kitchen? hungry
t7(T,M) :-
  basics:for(T,1,11),
  holds(loc(sumit,kitchen),T),
  holds(reason(sumit,M),T).

%Why did jason get the milk? thirsty
t8(T,M) :-
  basics:for(T,1,11),
  holds(holding(jason,milk),T),
  holds(reason(jason,M),T).

%Why did sumit get the apple? hungry
t9(T,M) :-
  basics:for(T,1,11),
  holds(holding(sumit,apple),T),
  holds(reason(sumit,M),T).

%Where will yann go? kitchen
t10(T,P) :-
	basics:for(T,1,11),
	holds(wantmove(yann,P),T).

%Why did yann go to the kitchen? thirsty
t11(T,M) :-
  basics:for(T,1,11),
  holds(loc(yann,kitchen),T),
  holds(reason(yann,M),T).
