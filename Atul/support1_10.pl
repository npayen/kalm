initiates(move(P,L), loc(P,L)).
terminates(move(P,L), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\=L.

    % the event EITHER places the person in both locations,
    % if queried, program should return MAYBE  rather than a definite yes/no
initiates(either(P,L1,_),loc(P,L1)).
initiates(either(P,_,L2),loc(P,L2)).
terminates(either(P,L1,L2),loc(P,L3)):-
    basics:member(L3,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L3\=L1, L3\=L2.

% if either holds, write maybe.
holds(either(P,L1,L2),T):-
    holds(loc(P,L1),T),
    holds(loc(P,L2),T),
    write(maybe).
    
% an event holds true b/w t1 and t2  if there is no other event at t3
% that terminates that event.
    
holds(Fact, Time2):-
    initiates(Event,Fact),
    happens(Event,Time1),
    Time1=<Time2,
    \+ (
	happens(Event2,Time3),
	terminates(Event2,Fact),
	Time1 =< Time3,
	Time3 =< Time2
	).
