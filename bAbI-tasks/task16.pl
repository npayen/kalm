:- if(current_prolog_flag(dialect,xsb)). %% MK: examples need it for portability
?- (current_module(metagol) -> true
   ; abort('Metagol must be loaded before examples.')
   ).
:- elif(current_prolog_flag(dialect,swi)).
:- use_module('metagol').
:- endif.
:- dynamic body_pred/1, head_pred/1.

/*
1 Lily is a swan.
2 Bernhard is a lion.
3 Greg is a swan.
4 Bernhard is white.
5 Brian is a lion.
6 Lily is gray.
7 Julius is a rhino.
8 Julius is gray.
9 Greg is gray.
10 What color is Brian?	white	5 2 4
*/

%% metagol settings
body_pred(swan/1).
body_pred(lion/1).
body_pred(rhino/1).

%% background knowledge
swan(lily).
lion(bernhard).
swan(greg).
lion(brian).
rhino(julius).

%% metarules
metarule([P,Q],[P,A],[[Q,A]]).
%metarule([P,Q],[P,A,B],[[Q,A,B]]).
%metarule([P,Q,R],[P,A,B],[[Q,A,B],[R,A,B]]).
%metarule([P,Q,R],[P,A,B],[[Q,A,C],[R,C,B]]).

%% learning gray/1
:- 
    %% positive examples
    Pos = [
        gray(lily),
        gray(julius)
        %gray(greg)
    ],
    
    Neg = [
        %gray(bernhard)
    ],
    learn(Pos,Neg).

%% learning white/1
:- 
    %% positive examples
    Pos = [
        white(bernhard),
        white(greg)
    ],
    
    Neg = [
        %white(lily)
        %white(lily),
        %white(julius),
    ],
    learn(Pos,Neg).


