:-ensure_loaded('support1_9.pl').

%1 Mary is no longer in the bedroom.
%2 Daniel moved to the hallway.
    %3 Is Mary in the bedroom? 	no	1

    happens(negation(mary,bedroom),1).
    happens(move(daniel,hallway),2).


    %3 Is Mary in the bedroom? 	no	1
    test(_):-
    basics:for(T,2,1),
    holds(loc(mary,bedroom),T),
    !.
    
