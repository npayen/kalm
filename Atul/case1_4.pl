:-ensure_loaded('support1_4.pl').

    
%1 The office is north of the kitchen.
%2 The garden is south of the kitchen.
%3 What is north of the kitchen?	office	1

north(office,kitchen).
south(garden,kitchen).


%3 What is north of the kitchen?	office	1
test1(X):-
    is_north(X,kitchen).
