initiates(move(P,L), loc(P,L)).
terminates(move(P,L), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\=L.
initiates(pickup(P,O), holding(P,O)).
terminates(putdown(P,O), holding(P,O)).


% transfer possesion tp(Object,Donor,Recipient)
%tp(O,D,R):-
 %   terminates(putdown(D,O),holding(D,O)),
  %  initiates(pickup(R,O), holding(R,O)).
terminates(tp(Object,Donor,_Recipient),holding(Donor,Object)).
initiates(tp(Object,_Donor,Recipient),holding(Recipient,Object)).

holds(tp(O,D,R), T):-
    holds(holding(R,O), T),
    \+holds(holding(D,O),T).    

% an object O is in a location L at frame T if the person P holding the object
% is also in the location L
holds(loc(O,L), T):-
    holds(holding(P,O), T),
    holds(loc(P,L), T).


% an event holds true b/w t1 and t2  if there is no other event at t3
% that terminates that event.
    
holds(Fact, Time2):-
    initiates(Event,Fact),
    happens(Event,Time1),
    Time1=<Time2,
    \+ (
	happens(Event2,Time3),
	terminates(Event2,Fact),
	Time1 =< Time3,
	Time3 =< Time2
	).
