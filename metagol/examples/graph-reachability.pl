:- if(current_prolog_flag(dialect,xsb)). %% need for portability
?- (current_module(metagol) -> true
   ; abort('Metagol must be loaded before examples.')
   ).
:- elif(current_prolog_flag(dialect,swi)).
:- use_module('../metagol').
:- endif.
:- dynamic body_pred/1, head_pred/1.

%% metagol settings
metagol:max_clauses(2).

%% tell metagol to use the BK
body_pred(edge/2).

%% metarules
metarule([P,Q], [P,A,B], [[Q,A,B]]).
metarule([P,Q], [P,A,B], [[Q,A,C], [P,C,B]]).

%% background knowledge
edge(a, b).
edge(b, c).
edge(c, a).
edge(a, d).



:-
  Pos = [p(a, b), p(a, c), p(a, a)],
  learn(Pos,[]).
