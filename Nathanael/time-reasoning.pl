/*
  Task 14: Time Reasoning
1 This morning Mary moved to the kitchen.
2 This afternoon Mary moved to the cinema.
3 Yesterday Bill went to the bedroom.
4 Yesterday Mary journeyed to the school.
5 Where was Mary before the cinema? 	kitchen	2 1
6 Yesterday Fred went back to the cinema.
7 Bill journeyed to the office this morning.
8 Where was Bill before the office? 	bedroom	7 3
9 Mary went to the school this evening.
10 This afternoon Bill journeyed to the kitchen.
11 Where was Bill before the office? 	bedroom	7 3
12 Julie went to the office yesterday.
13 This morning Fred journeyed to the office.
14 Where was Mary before the school? 	cinema	9 2
15 This evening Fred journeyed to the school.
16 This afternoon Fred journeyed to the bedroom.
17 Where was Mary before the school? 	cinema	9 2
*/

%How will we use time indicators like 'this morning', 'this afternoon', 'yesterday', etc. to indicate some sense of time?

happens(move(mary,kitchen),morning).
happens(move(mary,cinema),afternoon).
happens(move(bill,bedroom),yesterday).
happens(move(mary,school),yesterday).
happens(move(fred,cinema),yesterday).
happens(move(bill,office),morning).
happens(move(mary,school),evening).
happens(move(bill,kitchen),afternoon).
happens(move(julie,office),yesterday).
happens(move(fred,office),morning).
happens(move(fred,school),evening).
happens(move(fred,bedroom),afternoon).

time(yesterday,1).
time(morning,2).
time(afternoon,3).
time(evening,4).

initiates(move(P,L), loc(P,L)).
terminates(move(P,L), loc(P,L2)):-
	basics:member(L2,[cinema,kitchen,bedroom,school,office]),
	L2 \= L.

holds(Fact,Time2) :-
  initiates(Event1,Fact),
  happens(Event1,Time1),
  time(Time1,T1),
  time(Time2,T2),
  T1 =< T2,
  \+ (
    happens(Event2,Time3),
    terminates(Event2,Fact),
    time(Time3,T3),
    T1 =< T3,
    T3 =< T2
  ).

%Is Mary at the kitchen this afternoon? no
test1() :-
  holds(loc(mary,kitchen),afternoon).

%Where is Mary this afternoon? cinema
test2(L) :-
  holds(loc(mary,L),afternoon).

%Where was Mary before the cinema? kitchen, morning.
test3(Time2,L):-
	basics:for(T1,1,4),
  time(Time1,T1),
	holds(loc(mary,cinema),Time1),
	T2 is T1-1,
  time(Time2,T2),
	\+ holds(loc(mary,cinema),Time2),
	holds(loc(mary,L),Time2).

%Where was Bill before the office? bedroom, yesterday
test4(Time2,L):-
	basics:for(T1,1,4),
  time(Time1,T1),
	holds(loc(bill,office),Time1),
	T2 is T1-1,
  time(Time2,T2),
	\+ holds(loc(bill,office),Time2),
	holds(loc(bill,L),Time2).

%Where was Mary before the school? cinema, afternoon
test5(Time2,L):-
	basics:for(T1,1,4),
  time(Time1,T1),
	holds(loc(mary,school),Time1),
	T2 is T1-1,
  time(Time2,T2),
	\+ holds(loc(mary,school),Time2),
	holds(loc(mary,L),Time2).
