:-ensure_loaded('support1_13.pl').

%1 Mary and Daniel went to the bathroom.
%2 Then they journeyed to the hallway.
    %3 Where is Daniel? 	hallway	1 2


happens(doublemove(mary,daniel,bathroom),1).
happens(doublemove(mary,daniel,hallway),2).


%3 Where is Daniel? 	hallway	1 2
test(L):-
    basics:for(T,2,1),
    holds(loc(daniel,L),T),
    !.
