:-ensure_loaded('support1_8.pl').


%1 Mary moved to the bathroom.
%2 Sandra journeyed to the bedroom.
%3 Mary got the football there.
%4 John went to the kitchen.
    %5 What is Mary carrying? 	football	3
%6 John went to the bedroom.
%7 Mary dropped the football.
%8 What is Mary carrying? 	nothing	3 7


happens(move(mary,bathroom),1).
happens(move(sandra,bedroom),2).
happens(pickup(mary,football),3).
happens(move(john,kitchen),4).
happens(move(john,bedroom),5).
happens(putdown(mary,football),6).



%5 What is Mary carrying? 	football	3

test1(L):-
    carrying(mary,_,[L],4).


    %8 What is Mary carrying? 	nothing	3 7
test2(L):-
    carrying(mary,_,[L],6).
