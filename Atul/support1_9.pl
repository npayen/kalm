initiates(move(P,L), loc(P,L)).
terminates(move(P,L), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\=L.

    % the event Negation  makes the current location of a person untrue.
initiates(negation(P,L), loc(P,L2)):-
    L2\=L.
terminates(negation(P,L),loc(P,L)).    

% an event holds true b/w t1 and t2  if there is no other event at t3
% that terminates that event.
    
holds(Fact, Time2):-
    initiates(Event,Fact),
    happens(Event,Time1),
    Time1=<Time2,
    \+ (
	happens(Event2,Time3),
	terminates(Event2,Fact),
	Time1 =< Time3,
	Time3 =< Time2
	).
