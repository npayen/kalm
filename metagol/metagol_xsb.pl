:- set_global_compiler_options(allow_redefinition).

/*In Version 3.8, a module cannot export a predicate symbol that is directly imported from another module, since this would require that symbol to be in two modules. But one can import symbol1 from a module a symbol2 and then export symbol2. (And symbol1 and symbol2 are allowed to be the same symbol.*/

:- import
        reverse/2,
        flatten/2,
        memberchk/2,
        append/3,
        member/2,
        length/2,
        between/3
   from basics.
:- import format/2 from format.
:- import when/2 from constraintLib.
:- import parsort/4 from machine.
:- import term_expansion/2 from usermod.

:- import last/2 from lists.
:- import
        maplist/2,
        maplist/3,
        include/3
   from swi.
%%:- import succ/2 from c_arith.  %% MK: in the next release of XSB

:- import
        head_pred/1,
        body_pred/1,
        %%metarule/3,    %% MK: Not needed to be imported
        metarule/6
   from usermod.

%% min_clauses/1, max_clauses/1, max_inv_preds/1
%% are supposed to be undefined initially
%% metarule_next_id/1 - also!
%% Do not declare them dynamic, but local is ok!
:- local min_clauses/1, max_clauses/1, max_inv_preds/1, metarule_next_id/1.

:- export
        '@'/1.

%% This silences some warnings
:- export
        metasub_to_clause/2,
        atom_to_list/2,
        no_ordering/1,
        learn_task/2,
        pprint_clause/1.

atomic_list_concat(L,R) :- string:concat_atom(L,R).


/*
%% WILL REMOVE THIS succ/2 and properly_grounded/1
%% AFTER THE NEXT OFFICIAL XSB RELEASE.
%% Then import succ/2 from c_arith.
*/
%% from https://stackoverflow.com/questions/4234001/prolog-definition-of-succ-2
%% Bidirectionall successor; positive int
succ(N0, N1) :-
    ( properly_grounded(N0) ->  N1 is N0 + 1
    ; properly_grounded(N1) ->  N1 > 0, N0 is N1 - 1
    ; otherwise ->
        Ctx=context(succ/2,''),
        throw(error(instantiation_error,Ctx))
    ).
%% positive integer
properly_grounded(X):-
    (var(X) -> false
    ; 
        ( X >= 0 -> true
        ; otherwise ->
            Ctx = context(succ/2,X),
            E=domain_error(not_less_than_zero,X),
            throw(error(E,Ctx));otherwise
        )
    ).
%% end https://stackoverflow.com/questions/4234001/prolog-definition-of-succ-2

%% from lists.pl
%% XSB-ified pred from lists.pl
list_to_set(List, Set) :-
    %must_be(list, List),
    is_list(List),
    number_list(List, 1, Numbered),
    %sort(1, @=<, Numbered, ONum),
    parsort(Numbered,[asc(1)],0,ONum),
    remove_dup_keys(ONum, NumSet),
    %sort(2, @=<, NumSet, ONumSet),
    parsort(NumSet,[asc(2)],0,ONumSet),
    c_pairs:pairs_keys(ONumSet, Set).

number_list([], _, []).
number_list([H|T0], N, [H-N|T]) :-
    N1 is N+1,
    number_list(T0, N1, T).

remove_dup_keys([], []).
remove_dup_keys([H|T0], [H|T]) :-
    H = V-_,
    remove_same_key(T0, V, T1),
    remove_dup_keys(T1, T).

remove_same_key([V1-_|T0], V, T) :-
    V1 == V,
    !,
    remove_same_key(T0, V, T).
remove_same_key(L, _, L).
%% end of lists.pl



%%% Local Variables: 
%%% mode: prolog
%%% End: 
