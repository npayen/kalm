
:-ensure_loaded('support.pl').

% Mary moved to the bathroom.
% John went to the hallway.
% Where is Mary? 	bathroom	1

happens(move(mary,bathroom),1).
happens(move(john,hallway),2).


% query:
% Where is Mary? bathroom
test1(L):-
    basics:for(T,2,1),
    holds(loc(mary,L),T),
    !.
