%1 The red sphere is to the left of the yellow square.
%2 The red sphere is below the pink rectangle.
%3 Is the pink rectangle to the right of the yellow square?	no	2 1
%4 Is the yellow square to the right of the pink rectangle?	yes	1 2
%5 Is the pink rectangle to the left of the yellow square?	yes	2 1
%6 Is the yellow square below the pink rectangle?	yes	1 2
%7 Is the yellow square above the pink rectangle?	no	1 2
%8 Is the yellow square to the right of the pink rectangle?	yes	1 2
%9 Is the yellow square below the pink rectangle?	yes	1 2
    %10 Is the pink rectangle below the yellow square?	no	2 1

    left(redsphere,yellowsquare).
    below(redsphere,pinkrectangle).
right(X,Y):-
    left(Y,X).
above(X,Y):-
    below(Y,X).
is_above(X,Y):-
    above(X,Y);below(Y,X);
    above(X,Z),above(Z,Y);
    below(Y,Z),below(Z,X);
    above(X,Z),left(Z,Y);
    above(X,Z),right(Z,Y);
    right(X,Z),above(Z,Y);
    left(X,Z),above(Z,Y).

is_below(X,Y):-
    is_above(Y,X).

is_left(X,Y):-
    left(X,Y);right(Y,X);
    left(X,Z),left(Z,Y);
    right(Y,Z),right(Z,X);
    left(X,Z),above(Z,Y);
    left(X,Z),below(Z,Y);
    below(X,Z),left(Z,Y);
    above(X,Z),left(Z,Y).

is_right(X,Y):-
    is_left(Y,X).

%3 Is the pink rectangle to the right of the yellow square?	no	2 1
test1(_):-
    is_right(pinkrectangle,yellowsquare).

%4 Is the yellow square to the right of the pink rectangle?	yes	1 2
test2(_):-
    is_right(yellowsquare,pinkrectangle).

    %5 Is the pink rectangle to the left of the yellow square?	yes	2 1
test3(_):-
    is_left(pinkrectangle,yellowsquare).

    %6 Is the yellow square below the pink rectangle?	yes	1 2
test4(_):-
    is_below(yellowsquare,pinkrectangle).
