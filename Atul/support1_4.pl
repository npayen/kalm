
% An object X is north of object Y if Y is south of X,
% or X is north of some Z, which is north of Y.
is_north(X,Y):-
    south(Y,X);
    north(X,Y).


   
    
% an object X is south of Y if Y is north of X, or
% X is south of Z which is south of Y.
is_south(X,Y):-
    north(Y,X);
    south(X,Y).


