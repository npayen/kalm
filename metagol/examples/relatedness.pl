:- if(current_prolog_flag(dialect,xsb)). %% need for portability
?- (current_module(metagol) -> true
   ; abort('Metagol must be loaded before examples.')
   ).
:- elif(current_prolog_flag(dialect,swi)).
:- use_module('../metagol').
:- endif.
:- dynamic body_pred/1, head_pred/1.

%% tell Metagol to use the BK
body_pred(parent/2).

%% metarules
metarule([P,Q], [P,A,B], [[Q,A,B]]).
metarule([P,Q], [P,A,B], [[Q,B,A]]).
metarule([P,Q,R], [P,A,B], [[Q,A,C],[R,C,B]]).

%% background knowledge
parent(a,b).
parent(a,c).
parent(c,e).
parent(c,f).
parent(d,c).
parent(g,h).

:-
 Pos = [
    target(a,b),
    target(a,c),
    target(a,e),
    target(a,f),
    target(f,a),
    target(a,a),
    target(d,b),
    target(h,g)
  ],
  Neg = [
    target(g,a),
    target(a,h),
    target(e,g),
    target(g,b)
  ],
  learn(Pos,Neg).
