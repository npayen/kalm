

%1 The box of chocolates fits inside the chest.
%2 The box is bigger than the chest.
%3 The box is bigger than the suitcase.
%4 The suitcase fits inside the box.
%5 The container is bigger than the box of chocolates.
%6 Does the box fit in the box of chocolates?	no	1 2
%7 Is the box of chocolates bigger than the box?	no	1 2
%8 Is the box bigger than the box of chocolates?	yes	2 1
%9 Does the box of chocolates fit in the box?	yes	2 1
    %10 Does the box fit in the box of chocolates?	no	1 2

    fits(boxofchocolates,chest).
    bigger(box,chest).
    bigger(box,suitcase).
    fits(suitcase,box).
    bigger(container,boxofchocolates).

fit_in(X,Y):-
    fits(X,Y);
    fits(X,_),fits(_,Y);
    fits(X,_),bigger(Y,_);
    bigger(Y,X);
    bigger(Y,_),bigger(_,X);
    bigger(Y,_),fits(X,_).
    
    %6 Does the box fit in the box of chocolates?	no	1 2
test1(_):-
    fit_in(box,boxofchocolates).

    %7 Is the box of chocolates bigger than the box?	no	1 2
test2(_):-
    fit_in(box,boxofchocolates).

    %8 Is the box bigger than the box of chocolates?	yes	2 1
test3(_):-
    fit_in(boxofchocolates,box).

    %9 Does the box of chocolates fit in the box?	yes	2 1
test4(_):-
    fit_in(boxofchocolates,box).    
