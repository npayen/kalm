:-ensure_loaded('support1_10.pl').

%10 Bill is in the park.
%11 Bill is either in the office or the kitchen.
    %12 Is Bill in the office? 	maybe	11

    happens(move(bill,park),1).
    happens(either(bill,office,kitchen),2).

    %12 Is Bill in the office? 	maybe	11
test(_):-
    basics:for(T,2,1),
    holds(either(bill,office,_),T),
    !.
