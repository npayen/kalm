:- if(current_prolog_flag(dialect,xsb)). %% need for portability
?- (current_module(metagol) -> true
   ; abort('Metagol must be loaded before examples.')
   ).
:- elif(current_prolog_flag(dialect,swi)).
:- use_module('../metagol').
:- endif.
:- dynamic body_pred/1, head_pred/1.


%% metarules
metarule([P,A], [P,A,_B], []).
metarule([P,B], [P,_A,B], []).

:-
  Pos = [
    p(1,2),
    p(1,3),
    p(1,4),
    p(1,1),
    p(2,2),
    p(4,4)
    ],
  Neg = [
    p(2,4),
    p(3,4),
    p(3,1)
  ],
  learn(Pos,Neg).
