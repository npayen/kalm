initiates(move(P,L), loc(P,L)).
terminates(move(P,L), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\=L.
initiates(pickup(P,O), holding(P,O)).
terminates(putdown(P,O), holding(P,O)).


% creates list of objects being carried at the final time T.    
carrying(_,_,[],_).
carrying(P,Q,['nothing'],T):-
    \+ holds(holding(P,Q),T).
carrying(P,X,[X|Rest],T):-
    holds(holding(P,X),T),!,
    carrying(P,_,Rest,T).

% an event holds true b/w t1 and t2  if there is no other event at t3
% that terminates that event.
    
holds(Fact, Time2):-
    initiates(Event,Fact),
    happens(Event,Time1),
    Time1=<Time2,
    \+ (
	happens(Event2,Time3),
	terminates(Event2,Fact),
	Time1 =< Time3,
	Time3 =< Time2
	).
