
%1 The office is east of the hallway.
%2 The kitchen is north of the office.
%3 The garden is west of the bedroom.
%4 The office is west of the garden.
%5 The bathroom is north of the garden.
/*6 How do you go from the kitchen to the garden?	s,e	2 4*/

    pos(office,hallway,e).
    pos(kitchen,office,n).
    pos(garden,bedroom,w).
    pos(office,garden,w).
    pos(bathroom,garden,n).

pos(X,Y,s):-
    pos(Y,X,n).
    
    opposite(e,w).
    opposite(w,e).
    opposite(s,n).
    opposite(n,s).
    
oppos(B,A,Opp):-
    pos(A,B,Dir),
    opposite(Dir,Opp).
    
path(A,Z,Graph,Path,Final_Path):-
    path1(A,[Z],Graph,Path),
    basics:reverse(Path,Reversed_Path),
    direction(Reversed_Path,Final_Path).

path1(A,[A|Path1],_,[A|Path1]).

path1(A,[Y|Path1],Graph,Path):-
    adjacent(X,Y,Graph),
    \+ basics:member(X,Path1),
    path1(A,[X,Y|Path1],Graph,Path).

adjacent(X,Y,graph(_,Edges)):-
    basics:member(e(X,Y),Edges)
    ;
    basics:member(e(Y,X),Edges).
    
direction(Reversed_Path,Final_Path):-
    direction(Reversed_Path,[],Final_Path).
    
direction([_],Final_Path,Final_Path).
    
direction([X,Y|Rest],Part,Final_Path):-
    find_dir(X,Y,Z),
    direction([Y|Rest],[Z|Part],Final_Path).

find_dir(X,Y,Z):-
    pos(X,Y,Z);
    oppos(X,Y,Z).


 %6 How do you go from the kitchen to the garden?	s,e	2 4
test(Final_Path):-
    path(kitchen,garden,graph([office,kitchen,hallway,garden,bedroom,bathroom],[e(office,hallway),e(kitchen,office),e(garden,bedroom),e(office,garden),e(bathroom,garden)]),_,Final_Path).
