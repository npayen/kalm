:-ensure_loaded('support1_12.pl').

%1 Mary and Daniel travelled to the bathroom.
%2 John and Daniel travelled to the office.
    %3 Where is Daniel? 	office	2

    happens(doublemove(mary,daniel,bathroom),1).
    happens(doublemove(john,daniel,office),2).

test(L):-
    basics:for(T,2,1),
    holds(loc(daniel,L),T),
    !.
