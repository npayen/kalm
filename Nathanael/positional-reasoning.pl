/*
  Task 17: Positional Reasoning
1 The red square is below the blue square.
2 The red square is to the left of the pink rectangle.
3 Is the blue square below the pink rectangle?	no	1 2
4 Is the pink rectangle to the left of the blue square?	no	2 1
5 Is the blue square to the left of the pink rectangle?	yes	1 2
6 Is the pink rectangle to the left of the blue square?	no	2 1
7 Is the pink rectangle above the blue square?	no	2 1
8 Is the pink rectangle to the left of the blue square?	no	2 1
9 Is the pink rectangle above the blue square?	no	2 1
10 Is the blue square above the pink rectangle?	yes	1 2
*/

pos(redsquare,bluesquare,below).
pos(redsquare,pinkrectangle,left).

opposite(above,below).
opposite(below,above).
opposite(left,right).
opposite(right,left).

oppos(B,A,Opp) :-
  pos(A,B,Dir),
  opposite(Dir,Opp).

above1(A,B) :-
  pos(A,B,above);
  oppos(A,B,above).

above2(A,B) :-
  above1(A,C),
  \+ above2(C,B).

below1(A,B) :-
  pos(A,B,below);
  oppos(A,B,below).

below2(A,B) :-
  below1(A,C),
  \+ below2(C,B).

left1(A,B) :-
  pos(A,B,left);
  oppos(A,B,left).

left2(A,B) :-
  left1(A,C),
  \+ left2(C,B).

right1(A,B) :-
  pos(A,B,right);
  oppos(A,B,right).

right2(A,B) :-
  right1(A,C),
  \+ right2(C,B).

is_above(A,B) :-
  above1(A,B);
  above2(A,B);
  below2(B,A).

is_below(A,B) :-
  below1(A,B);
  below2(A,B);
  above2(B,A).

is_left(A,B) :-
  left1(A,B);
  left2(A,B);
  right2(B,A).

is_right(A,B) :-
  right1(A,B);
  right2(A,B);
  left2(B,A).

%Is the blue square below the pink rectangle?	no
test1() :-
  is_below(bluesquare,pinkrectangle).

%Is the pink rectangle to the left of the blue square?	no
test2() :-
  is_left(pinkrectangle,bluesquare).

%Is the blue square to the left of the pink rectangle?	yes
test3() :-
  is_left(bluesquare,pinkrectangle).

%Is the pink rectangle to the left of the blue square?	no
test4() :-
  is_left(pinkrectangle,bluesquare).

%Is the pink rectangle above the blue square?	no
test5() :-
  is_above(pinkrectangle,bluesquare).

%Is the pink rectangle to the left of the blue square?	no
test6() :-
  is_left(pinkrectangle,bluesquare).

%Is the pink rectangle above the blue square?	no
test7() :-
  is_above(pinkrectangle,bluesquare).

%Is the blue square above the pink rectangle?	yes
test8() :-
  is_above(bluesquare,pinkrectangle).
