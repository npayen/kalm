% an object X is east of Y if Y is west of X, or
% X is east of some Z, and Z is east of Y.
is_east(X,Y):-
    west(Y,X);
    east(X,Y).


    
% an object X is west of Y if Y is east of X, or
% X is west of Z and Z is west of Y.
is_west(X,Y):-
    east(Y,X);
    west(X,Y).
 
