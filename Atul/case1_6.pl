:-ensure_loaded('support.pl').


%1 Mary moved to the bathroom.
%2 Sandra journeyed to the bedroom.
    %3 Is Sandra in the hallway? 	no	2

happens(move(mary,bathroom),1).
happens(move(sandra,bedroom),2).
    
%3 Is Sandra in the hallway? 	no	2

test1(_):-
    basics:for(T,2,1),
    holds(loc(sandra,hallway),T),
    !.
