:- if(current_prolog_flag(dialect,xsb)). %% need for portability
?- (current_module(metagol) -> true
   ; abort('Metagol must be loaded before examples.')
   ).
:- elif(current_prolog_flag(dialect,swi)).
:- use_module('../metagol').
:- endif.
:- dynamic body_pred/1, head_pred/1.

%% tell Metagol to use the BK
body_pred(edge/2).

%% metarules
metarule([P,Q], [P,A,B], [[Q,A,B]]).
metarule([P,Q], [P,A,B], [[Q,B,A]]).

%% background knowledge
edge(a,b).
edge(b,d).
edge(c,c).

:-
 Pos = [
    target(a,b),
    target(b,a),
    target(b,d),
    target(d,b),
    target(c,c)
  ],
  Neg = [
  ],
  learn(Pos,Neg).
