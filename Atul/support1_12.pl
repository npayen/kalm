initiates(move(P,L), loc(P,L)).
terminates(move(P,L), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\=L.



    % the event DOUBLEMOVE moves two people to the same place.
initiates(doublemove(P1,_,L),loc(P1,L)).
initiates(doublemove(_,P2,L),loc(P2,L)).
terminates(doublemove(P1,_,L),loc(P1,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\=L.
terminates(doublemove(_,P2,L),loc(P2,L3)):-
    basics:member(L3,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L3\=L.

holds(doublemove(P1,P2,L),T):-
    holds(loc(P1,L),T),
    holds(loc(P2,L),T).

    % an event holds true b/w t1 and t2  if there is no other event at t3
% that terminates that event.
    
holds(Fact, Time2):-
    initiates(Event,Fact),
    happens(Event,Time1),
    Time1=<Time2,
    \+ (
	happens(Event2,Time3),
	terminates(Event2,Fact),
	Time1 =< Time3,
	Time3 =< Time2
	).
