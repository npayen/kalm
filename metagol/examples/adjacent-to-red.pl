:- if(current_prolog_flag(dialect,xsb)). %% need for portability
?- (current_module(metagol) -> true
   ; abort('Metagol must be loaded before examples.')
   ).
:- elif(current_prolog_flag(dialect,swi)).
:- use_module('../metagol').
:- endif.
:- dynamic body_pred/1, head_pred/1.


%% tell Metagol to use the BK
body_pred(edge/2).
body_pred(colour/2).
body_pred(red/1).
body_pred(green/1).

%% metarules
metarule([P,Q,R], [P,A], [[Q,A,B],[R,B]]).

%% background knowledge
edge(a,b).
edge(b,a).
edge(c,d).
edge(c,e).
edge(d,e).
colour(a,red).
colour(b,green).
colour(c,red).
colour(d,red).
colour(e,green).
red(red).
green(green).

a:-
 Pos = [
    target(b),
    target(c)
  ],
  Neg = [
    target(a),
    target(d),
    target(e)
  ],
  learn(Pos,Neg).

%% body_pred(mother/2).
%% body_pred(father/2).

%% body_pred(shoe/1).

%% %% metarules
%% metarule([P,Q], [P,A,B], [[Q,A,B]]).
%% metarule([P,Q,R], [P,A,B], [[Q,A,C],[R,C,B]]).
