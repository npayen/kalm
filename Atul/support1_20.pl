initiates(move(P,L), loc(P,L)).
terminates(move(P,L), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\=L.
initiates(pickup(P,O), holding(P,O)).
terminates(putdown(P,O), holding(P,O)).

% an object O is in a location L at frame T if the person P holding the object
% is also in the location L
holds(loc(O,L), T):-
    holds(holding(P,O), T),
    holds(loc(P,L), T).

% feeling THIRSTY inititaes a movement to the kitchen
initiates(feeling(P,thirsty),loc(P,kitchen)).
terminates(feeling(P,thirsty), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\='kitchen'.

    % feeling bored intiates a movement to the garden
initiates(feeling(P,bored),loc(P,garden)).
terminates(feeling(P,bored), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\='garden'.

    % feeling hungry initiates a movement to the kitchen.
initiates(feeling(P,hungry),loc(P,kitchen)).
terminates(feeling(P,hungry), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\='kitchen'.

    % feeling tired initiates a movement to the bedroom.
initiates(feeling(P,tired),loc(P,bedroom)).
terminates(feeling(P,tired), loc(P,L2)):-
    basics:member(L2,[bedroom,bathroom,kitchen,garden,office,hallway]),
    L2\='bedroom'.

    % any 'WHY' question should return the persons emotion.
why(P,Time,Answer):-
    happens(feeling(P,Answer),Time1),
    Time1=<Time.
    
% an event holds true b/w t1 and t2  if there is no other event at t3
% that terminates that event.
    
holds(Fact, Time2):-
    initiates(Event,Fact),
    happens(Event,Time1),
    Time1=<Time2,
    \+ (
	happens(Event2,Time3),
	terminates(Event2,Fact),
	Time1 =< Time3,
	Time3 =< Time2
	).
