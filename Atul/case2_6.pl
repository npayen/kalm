:-ensure_loaded('support.pl').


%4 Mary moved to the hallway.
%5 Daniel journeyed to the garden.
    %6 Is Mary in the hallway? 	yes	4


happens(move(mary,hallway),1).
happens(move(daniel,garden),2).

    %6 Is Mary in the hallway? 	yes	4
test1(_):-
    basics:for(T,2,1),
    holds(loc(mary,hallway),T),
    !.    
