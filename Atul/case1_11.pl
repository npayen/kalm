:-ensure_loaded('support.pl').

%1 Mary went back to the bathroom.
%2 After that she went to the bedroom.
    %3 Where is Mary? 	bedroom	1 2

happens(move(mary,bathroom),1).
happens(move(mary,bedroom),2).

%3 Where is Mary? 	bedroom	1 2
    test(L):-
    basics:for(T,2,1),
    holds(loc(mary,L),T),
    !.

