:-ensure_loaded('support1_2.pl').


% Mary moved to the bathroom.
% Sandra journeyed to the bedroom.
% Mary got the football there.
% John went to the kitchen.
% Mary went back to the kitchen.
% Mary went back to the garden.
% Where is the football? 	garden	3 6
% Sandra went back to the office.
% John moved to the office.
% Sandra journeyed to the hallway.
% Daniel went back to the kitchen.
% Mary dropped the football.
% John got the milk there.
% Where is the football? 	garden	12 6

happens(move(mary,bathroom),1).
happens(move(sandra,bedroom),2).
happens(pickup(mary,football),3).
happens(move(john,kitchen),4).
happens(move(mary,kitchen),5).
happens(move(mary,garden),6).

happens(move(sandra,office),8).
happens(move(john,office),9).
happens(move(daniel,kitchen),10).
happens(putdown(mary,football),11).
happens(pickup(john,milk),12).
    

% Where is the football? 	garden	3 6
test1(L):-
    basics:for(T,6,1),
    holds(loc(football,L),T),
    !.

% Where is the football? 	garden	12 6
test2(L2):-
    basics:for(T1,12,1),
    holds(loc(football,L2),T1),
    !.
