:-ensure_loaded('support1_20.pl').

%1 Sumit is tired.
%2 Where will sumit go?	bedroom	1
%3 Sumit went back to the bedroom.
%4 Why did sumit go to the bedroom?	tired	1
%5 Sumit grabbed the pajamas there.
%6 Why did sumit get the pajamas?	tired	1
%7 Yann is bored.
%8 Where will yann go?	garden	7
%9 Jason is thirsty.
%10 Where will jason go?	kitchen	9
%11 Yann travelled to the garden.
%12 Why did yann go to the garden?	bored	7
%13 Yann got the football there.
%14 Why did yann get the football?	bored	7
%15 Jason went back to the kitchen.
%16 Why did jason go to the kitchen?	thirsty	9
%17 Antoine is thirsty.
%18 Where will antoine go?	kitchen	17
%19 Jason grabbed the milk there.
%20 Why did jason get the milk?	thirsty	9
%21 Antoine travelled to the kitchen.
%22 Why did antoine go to the kitchen?	thirsty	17

happens(feeling(sumit,tired),1).
happens(move(sumit,bedroom),2).
happens(pickup(sumit,pajamas),3).
happens(feeling(yann,bored),4).
happens(feeling(jason,thirsty),5).
happens(move(yann,garden),6).
happens(pickup(yann,football),7).
happens(move(jason,kitchen),8).
happens(feeling(antoine,thirsty),9).
happens(pickup(jason,milk),10).
happens(move(antoine,kitchen),11).

    %2 Where will sumit go?	bedroom	1
test1(L):-
    basics:for(T,1,2),
    holds(loc(sumit,L),T).

    %4 Why did sumit go to the bedroom?	tired	1
test2(Answer):-
    why(sumit,2,Answer).

    %6 Why did sumit get the pajamas?	tired	1
test3(Answer):-
    why(sumit,3,Answer).

    %8 Where will yann go?	garden	7
test4(L):-
    basics:for(T,1,4),
    holds(loc(yann,L),T).

    %10 Where will jason go?	kitchen	9
test5(L):-
    basics:for(T,1,5),
    holds(loc(jason,L),T).

    %12 Why did yann go to the garden?	bored	7
test6(Answer):-
    why(yann,6,Answer).
