:- if(current_prolog_flag(dialect,xsb)). %% need for portability
?- (current_module(metagol) -> true
   ; abort('Metagol must be loaded before examples.')
   ).
:- elif(current_prolog_flag(dialect,swi)).
:- use_module('../metagol').
:- endif.
:- dynamic body_pred/1, head_pred/1.

%% tell Metagol to use the BK
body_pred(edge/2).

%% metarules
metarule(ident, [P,Q], [P,A,B], [[Q,A,B]]).
metarule(chain, [P,Q,R], [P,A,B], [[Q,A,C],[R,C,B]]).
metarule(tailrec, [P,Q], [P,A,B], [[Q,A,C],[P,C,B]]).

%% background knowledge
edge(a,b).
edge(b,c).
edge(c,d).
edge(b,a).

:-
 Pos = [
    target(a,b),
    target(b,c),
    target(c,d),
    target(b,a),
    target(a,c),
    target(a,d),
    target(a,a),
    target(b,d),
    target(b,a),
    target(b,b)
  ],
  Neg = [
  ],
  learn(Pos,Neg).
