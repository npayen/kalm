:-ensure_loaded('support1_3.pl').

% 1 Mary moved to the bathroom.
% 2 Sandra journeyed to the bedroom.
% 3 Mary got the football there.
% 4 John went back to the bedroom.
% 5 Mary journeyed to the office.
% 6 John journeyed to the office.
% 7 John took the milk.
% 8 Daniel went back to the kitchen.
% 9 John moved to the bedroom.
% 10 Daniel went back to the hallway.
% 11 Daniel took the apple.
% 12 John left the milk there.
% 13 John travelled to the kitchen.
% 14 Sandra went back to the bathroom.
% 15 Daniel journeyed to the bathroom.
% 16 John journeyed to the bathroom.
% 17 Mary journeyed to the bathroom.
% 18 Sandra went back to the garden.
% 19 Sandra went to the office.
% 20 Daniel went to the garden.
% 21 Sandra went back to the hallway.
% 22 Daniel journeyed to the office.
% 23 Mary dropped the football.
% 24 John moved to the bedroom.
% 25 Where was the football before the bathroom? 	office	23 17 5    

    
happens(move(mary,bathroom),1).
happens(move(sandra,bedroom),2).
happens(pickup(mary,football),3).
happens(move(john,bedroom),4).
happens(move(mary,office),5).
happens(move(john,office),6).
happens(pickup(john,milk),7).
happens(move(daniel,kitchen),8).
happens(move(john,bedroom),9).
happens(move(daniel,hallway),10).
happens(pickup(daniel,apple),11).
happens(putdown(john,milk),12).
happens(move(john,kitchen),13).
happens(move(sandra,bathroom),14).
happens(move(daniel,bathroom),15).
happens(move(john,bathroom),16).
happens(move(mary,bathroom),17).
happens(move(sandra,garden),18).
happens(move(sandra,office),19).
happens(move(daniel,garden),20).
happens(move(sandra,hallway),21).
happens(move(daniel,office),22).
happens(putdown(mary,football),23).
happens(move(john,bedroom),24).


% 25 Where was the football before the bathroom? 	office	23 17 5
test1(T2,L):-
	basics:for(T1,1,24),
	holds(loc(football,bathroom), T1),
	T2 is T1-1,
	\+ holds(loc(football,bathroom), T2),
	holds(loc(football,L),T2).
