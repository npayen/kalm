:-ensure_loaded('support1_7').



%    1 Mary moved to the bathroom.
%2 Sandra journeyed to the bedroom.
%3 John went to the kitchen.
%4 Mary took the football there.
%5 How many objects is Mary carrying? 	one	4


   
happens(move(mary,bathroom),1).
happens(move(sandra,bedroom),2).
happens(move(john,kitchen),3).
happens(pickup(mary,football),4).

%5 How many objects is Mary carrying? 	one	4
test1(_):-
    carrying(mary,_,[L],4),
    basics:length([L],Y),
    full_word(Y).
    
full_word(Y):-
    Y==0,write(zero);
    Y==1,write(one);
    Y==2,write(two).
    

